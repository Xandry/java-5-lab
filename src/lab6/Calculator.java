package lab6;

import lab6.listener.MyActionListener;
import lab6.listener.MyItemListener;
import lab6.listener.MyWindowListener;

import java.awt.*;
import java.awt.event.*;

public class Calculator extends Frame {

    private TextField textField1;
    private TextField textField2;
    private TextField resultTextField;
    private Choice choice;

    private ItemListener itemListener = new MyItemListener();
    private WindowListener windowListener = new MyWindowListener();
    private ActionListener actionListener;

    public Calculator(String title) throws HeadlessException {
        super(title);
        setLayout(null);
        setSize(600, 300);
        addWindowListener(windowListener);

        choice = new Choice();
        choice.add("Add");
        choice.add("Multiply");
        choice.add("Subtract");
        choice.add("Divide");
        choice.addItemListener(itemListener);
        choice.setBounds(175, 100, 50, 20);
        add(choice);

        textField1 = new TextField(15);
        textField1.setBounds(50, 100, 100, 20);
        add(textField1);

        textField2 = new TextField(15);
        textField2.setBounds(250, 100, 100, 20);
        add(textField2);

        resultTextField = new TextField(15);
        resultTextField.setBounds(400, 140, 100, 20);
        resultTextField.setEditable(false);
        add(resultTextField);

        actionListener = new MyActionListener(textField1, textField2, resultTextField, choice);

        Button calculateButton = new Button("Calculate");
        calculateButton.addActionListener(actionListener);
        calculateButton.setBounds(400, 100, 60, 20);
        add(calculateButton);
    }

}
