package lab6.listener;

import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MyActionListener implements ActionListener {

    private TextField textField1;
    private TextField textField2;
    private TextField result;
    private Choice choice;

    public MyActionListener(TextField textField1, TextField textField2, TextField result, Choice choice) {
        this.textField1 = textField1;
        this.textField2 = textField2;
        this.result = result;
        this.choice = choice;
    }

    @Override
    public void actionPerformed(ActionEvent actionEvent) {
        try {
            Double number1 = Double.valueOf(textField1.getText());
            Double number2 = Double.valueOf(textField2.getText());
            Double resultNumber = null;

            if (number2 != 0) {
                switch (choice.getSelectedIndex()) {
                    case 0:
                        resultNumber = number1 + number2;
                        break;
                    case 1:
                        resultNumber = number1 * number2;
                        break;
                    case 2:
                        resultNumber = number1 - number2;
                        break;
                    case 3:
                        resultNumber = number1 / number2;
                        break;
                }
            } else {
                throw new IllegalArgumentException("Divide by zero");
            }
            result.setText(String.valueOf(resultNumber));
        } catch (IllegalArgumentException e) {
            result.setText("Error");
        }

    }
}
