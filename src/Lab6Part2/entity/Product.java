package Lab6Part2.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.Date;

public class Product implements Cloneable, Serializable {
    private String name;
    private boolean isFragile;
    private LocalDate receiptDate;

    public Product(String name, boolean isFragile, LocalDate receiptDate) {
        this.name = name;
        this.isFragile = isFragile;
        this.receiptDate = receiptDate;
    }

    public String getName() {
        return name;
    }

    public boolean isFragile() {
        return isFragile;
    }

    public LocalDate getReceiptDate() {
        return receiptDate;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Product{");
        sb.append("name='").append(name).append('\'');
        sb.append(", isFragile=").append(isFragile);
        sb.append(", receiptDate=").append(receiptDate);
        sb.append('}');
        return sb.toString();
    }
}
