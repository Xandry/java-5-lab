package Lab6Part2;

import Lab6Part2.entity.Product;
import Lab6Part2.listener.AddProductButtonListener;
import Lab6Part2.listener.ReadFromFileButtonListener;
import Lab6Part2.listener.SaveToFileButtonListener;

import javax.swing.*;
import java.awt.*;

public class ProductRegistration extends JFrame {

    private final int FIRST_LINE = 50;
    private final int FIRST_COLUMN = 20;
    private JTextField nameTextField;
    private JLabel nameLabel;
    private JCheckBox isFragileCheckBox;
    private JButton addButton;
    private JButton readFromFileButton;
    private JButton saveToFileButton;
    private JList<Product> productList;
    private DefaultListModel<Product> productDefaultListModel;
    private AddProductButtonListener addProductButtonListener;
    private ReadFromFileButtonListener readFromFileButtonListener;
    private SaveToFileButtonListener saveToFileButtonListener;

    public ProductRegistration() throws HeadlessException {
        super("Registration");
        setLayout(null);
        setSize(500, 500);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        nameTextField = new JTextField(30);
        nameTextField.setSize(140, 20);
        nameTextField.setLocation(FIRST_COLUMN, FIRST_LINE);
        add(nameTextField);

        nameLabel = new JLabel("Name");
        nameLabel.setSize(80, 20);
        nameLabel.setLocation(nameTextField.getX(), nameTextField.getY() - 20);
        add(nameLabel);

        isFragileCheckBox = new JCheckBox("Fragile");
        isFragileCheckBox.setSize(80, 30);
        isFragileCheckBox.setLocation(nameTextField.getX() + nameTextField.getWidth() + 20, nameTextField.getY() - 5);
        add(isFragileCheckBox);

        productDefaultListModel = new DefaultListModel<>();
        productList = new JList<>(productDefaultListModel);
        productList.setSize(450, 250);
        productList.setLocation(FIRST_COLUMN, FIRST_LINE + 80);
        add(productList);

        addProductButtonListener = new AddProductButtonListener(nameTextField, isFragileCheckBox, productList);

        addButton = new JButton("Add");
        addButton.setLocation(isFragileCheckBox.getX() + 100, isFragileCheckBox.getY());
        addButton.setSize(80, 30);
        addButton.addActionListener(addProductButtonListener);
        add(addButton);

        saveToFileButtonListener = new SaveToFileButtonListener(productList);
        readFromFileButtonListener = new ReadFromFileButtonListener(productList);

        saveToFileButton = new JButton("Save to file");
        saveToFileButton.setLocation(productList.getX(), productList.getY() + productList.getHeight() + 20);
        saveToFileButton.setSize(150, 30);
        saveToFileButton.addActionListener(saveToFileButtonListener);
        add(saveToFileButton);

        readFromFileButton = new JButton("Read from file");
        readFromFileButton.setLocation(saveToFileButton.getX() + saveToFileButton.getWidth() + 20, saveToFileButton.getY());
        readFromFileButton.setSize(150, 30);
        readFromFileButton.addActionListener(readFromFileButtonListener);
        add(readFromFileButton);

    }
}
