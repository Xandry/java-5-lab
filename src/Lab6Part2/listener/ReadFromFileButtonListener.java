package Lab6Part2.listener;

import Lab6Part2.entity.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;

public class ReadFromFileButtonListener implements ActionListener {

    private JList<Product> productJList;

    public ReadFromFileButtonListener(JList<Product> productJList) {
        this.productJList = productJList;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    new FileInputStream("products.out"));
            productJList.setModel((DefaultListModel<Product>) objectInputStream.readObject());
            objectInputStream.close();
        } catch (IOException | ClassNotFoundException ex) {
            ex.printStackTrace();
        }
    }
}
