package Lab6Part2.listener;

import Lab6Part2.entity.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectOutputStream;

public class SaveToFileButtonListener implements ActionListener {

    private JList<Product> productJList;

    public SaveToFileButtonListener(JList<Product> productJList) {
        this.productJList = productJList;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    new FileOutputStream("products.out"));
            objectOutputStream.writeObject(productJList.getModel());
            objectOutputStream.close();
        } catch (IOException ex) {
            ex.printStackTrace();
        }
    }
}
