package Lab6Part2.listener;

import Lab6Part2.entity.Product;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.time.LocalDate;

public class AddProductButtonListener implements ActionListener {

    private JTextField nameField;
    private JCheckBox isFragileCheckBox;
    private JList<Product> productList;

    public AddProductButtonListener(JTextField nameField, JCheckBox isFragileCheckBox, JList<Product> productList) {
        this.nameField = nameField;
        this.isFragileCheckBox = isFragileCheckBox;
        this.productList = productList;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
        String name = nameField.getText();
        if (!name.isBlank()) {
            boolean isFragile = isFragileCheckBox.isSelected();
            Product product = new Product(name, isFragile, LocalDate.now());
            System.out.println(product);
            ((DefaultListModel<Product>) productList.getModel()).addElement(product);
        }
    }
}
